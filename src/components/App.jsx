import React from "react";

import { AppContainer } from "assets/styles/App.styles.jsx";

import Display from "components/Display.jsx";

const App = () => {
  return (
    <AppContainer>
      <Display />
    </AppContainer>
  );
};

export default App;
