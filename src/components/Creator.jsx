import React, { useState, useEffect, useRef } from "react";
// import { useParams } from "react-router-dom";

import constants from "assets/constants.js";

import {
  CreatorContainer,
  CreatorDownload,
} from "assets/styles/Creator.styles.jsx";

import { generateFile } from "actions/generateFile";

const Creator = () => {
  const [download, setDownload] = useState(null);

  const drop = useRef(null);
  const files = useRef([]);

  useEffect(() => {
    drop.current.addEventListener("dragover", handleDragOver);
    drop.current.addEventListener("drop", handleDrop);

    return () => {
      drop.current.removeEventListener("dragover", handleDragOver);
      drop.current.removeEventListener("drop", handleDrop);
    };
  }, []);

  const handleDragOver = (e) => {
    e.preventDefault();
    e.stopPropagation();
  };

  const handleDrop = (e) => {
    e.preventDefault();
    e.stopPropagation();

    const droppedFiles = [...e.dataTransfer.files];

    if (droppedFiles && droppedFiles.length === 1) onUpload(droppedFiles[0]);
  };

  const onUpload = (file) => {
    const extension = constants.orderExt[files.current.length];

    if (!file.name.endsWith(extension)) throw `FILE NOT OF ${extension} FORMAT`;

    files.current = [...files.current, file];

    console.log(files.current);

    if (files.current.length === constants.order.length) {
      const randomName =
        parseInt(Math.random() * 10000000) + constants.globalExt;
      generateFile(files.current).then((blob) =>
        setDownload(new File([blob], randomName))
      );
    }
  };

  return (
    <CreatorContainer ref={drop}>
      {download && (
        <CreatorDownload
          href={window.URL.createObjectURL(download)}
          download={download.name}
        >
          DOWNLOAD FILE
        </CreatorDownload>
      )}
    </CreatorContainer>
  );
};

export default Creator;
