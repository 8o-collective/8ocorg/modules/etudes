import React from "react";

import constants from "assets/constants";

import { CallsContainer, CallsText } from "assets/styles/Rack/Calls.styles.jsx";

const Calls = ({ code }) => {
  return (
    <CallsContainer>
      <CallsText>
        {code
          .slice(-constants.maxLines)
          .map((statement, i) =>
            statement ? <div key={i}>{`${statement};`}</div> : <br key={i} />
          )}
      </CallsText>
    </CallsContainer>
  );
};

export default Calls;
