import React, { useState, useEffect, useRef } from "react";
import Oscilloscope from "oscilloscope";

import {
  WaveformContainer,
  WaveformCanvas,
  WaveformAudio,
  WaveformInformation,
} from "assets/styles/Rack/Waveform.styles.jsx";

const Waveform = ({ data, information }) => {
  const [scope, setScope] = useState(null);

  console.log(scope);

  const audioRef = useRef(null);
  const canvasRef = useRef(null);

  useEffect(() => {
    if (data.context) {
      setScope(() => {
        const source = data.context.createMediaElementSource(audioRef.current);

        const s = new Oscilloscope(source);
        source.connect(data.context.destination);

        const ctx = canvasRef.current.getContext("2d");
        ctx.lineWidth = 1;
        ctx.strokeStyle = "red";

        s.animate(ctx);
        audioRef.current.play();
        return s;
      });
    }
  }, [data]);

  return (
    <WaveformContainer>
      <WaveformInformation
        bpm={information.bpm}
        samplerate={information.samplerate}
        amplitude={data.amplitude}
      />
      <WaveformCanvas ref={canvasRef} />
      <WaveformAudio autoplay ref={audioRef} src={data.raw} />
    </WaveformContainer>
  );
};

export default Waveform;
