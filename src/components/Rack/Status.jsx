import React from "react";

import constants from "assets/constants";

import {
  StatusContainer,
  StatusText,
} from "assets/styles/Rack/Status.styles.jsx";

const Status = ({ text }) => {
  return (
    <StatusContainer>
      <StatusText>
        {text
          .slice(-constants.maxLines)
          .map((content, i) =>
            content ? <div key={i}>{`> ${content}`}</div> : <br key={i} />
          )}
      </StatusText>
    </StatusContainer>
  );
};

export default Status;
