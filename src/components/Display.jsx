import React, { useEffect, useRef, useState } from "react";

import { extension } from "actions/getExtension.js";
import { checkFile, loadFile } from "actions/loadFile.js";

import constants from "assets/constants";

import { DisplayContainer } from "assets/styles/Display.styles.jsx";

import Calls from "components/Rack/Calls.jsx";
import Status from "components/Rack/Status.jsx";
import Waveform from "components/Rack/Waveform.jsx";

const Display = () => {
  const [status, setStatus] = useState([constants.defaults.status]);
  const [calls, setCalls] = useState([constants.defaults.calls]);
  const [data, setData] = useState({ amplitude: 0 });
  const [information, setInformation] = useState({ bpm: 0, samplerate: 0 });

  const pushStatus = (content) =>
    setStatus((statusState) => [...statusState, content]);
  const pushCall = (code) => setCalls((callState) => [...callState, code]);
  const pushCallEnsureNoRepeat = (code) =>
    setCalls((callState) =>
      callState.slice(-1)[0] !== code ? [...callState, code] : callState
    );

  const drop = useRef(null);

  useEffect(() => {
    drop.current.addEventListener("dragover", handleDragOver);
    drop.current.addEventListener("drop", handleDrop);

    return () => {
      drop.current.removeEventListener("dragover", handleDragOver);
      drop.current.removeEventListener("drop", handleDrop);
    };
  }, []);

  const handleDragOver = (e) => {
    e.preventDefault();
    e.stopPropagation();

    pushCallEnsureNoRepeat("prepareForLoad()");
  };

  const handleDrop = (e) => {
    e.preventDefault();
    e.stopPropagation();

    const files = [...e.dataTransfer.files];

    if (files) onUpload(files[0]);
  };

  const onUpload = (file) => {
    console.log(file);
    if (
      !file.name.endsWith(`.${extension}`) &&
      !file.name.endsWith(constants.globalExt)
    )
      return pushStatus(`FILE NOT OF .${extension} FORMAT`);
    pushCall(`load(${file.name})`);

    const reader = new FileReader();
    reader.readAsArrayBuffer(file);
    reader.onload = (e) => {
      try {
        var data = checkFile(e.target.result);
      } catch (e) {
        return pushStatus(e);
      }

      // animate both going up, then we can interpret the actual file
      let printCount = 0;
      const printInterval = setInterval(() => {
        pushStatus("");
        pushCall("");
        printCount++;

        if (printCount === 10) {
          setCalls([]);
          setStatus([]);

          clearInterval(printInterval);
          loadFile(data, pushStatus, pushCall, setData, setInformation);
        }
      }, 200);
    };

    reader.onerror = () => {
      document.getElementById("fileContents").innerHTML = "error reading file";
    };
  };

  return (
    <DisplayContainer ref={drop}>
      <Calls code={calls} />
      <Status text={status} />
      <Waveform data={data} information={information} />
    </DisplayContainer>
  );
};

export default Display;
