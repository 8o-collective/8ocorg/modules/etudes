import React from "react";
import { Routes, Route } from "react-router-dom";

import App from "components/App.jsx";
import Creator from "components/Creator.jsx";

const routes = (
  <Routes>
    <Route path="*" element={<App />} />
    <Route path="/creator" element={<Creator />} />
  </Routes>
);

export { routes };
