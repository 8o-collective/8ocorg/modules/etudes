export default {
  maxLines: 10,

  fileHeader: new TextEncoder().encode("8oCetdsf"), // 8oC etudes file header
  headerSize: 20,

  order: ["audio", "midi", "comments"],
  orderExt: [".ogg", ".mid", ".csv"],

  globalExt: ".etd",

  defaults: {
    status: "PLEASE INSERT DISK",
    calls: "waitForLoad()",
  },
};
