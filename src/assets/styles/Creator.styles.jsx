import styled from "styled-components";

const CreatorContainer = styled.div`
  margin: 0px;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;

  overflow-y: hidden;

  background-color: #000000;
  opacity: 1;
  background-image: repeating-radial-gradient(
      circle at center center,
      transparent 0,
      #000000 30px
    ),
    repeating-linear-gradient(#ff000055, #ff0000);
`;

const CreatorDownload = styled.a`
  margin: 0px;
  position: absolute;
  width: 100%;
  top: 50vh;
  transform: translateY(-50%);

  font-family: IBM3270;
  color: white;
  font-size: 10vw;
  text-align: center;
`;

export { CreatorContainer, CreatorDownload };
