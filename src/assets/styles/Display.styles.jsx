import styled from "styled-components";

const DisplayContainer = styled.div`
  position: absolute;
  width: 80vw;
  aspect-ratio: 3 / 1;
  top: 50vh;
  left: 50vw;
  transform: translate(-50%, -50%);

  border: solid 1px red;
`;

export { DisplayContainer };
