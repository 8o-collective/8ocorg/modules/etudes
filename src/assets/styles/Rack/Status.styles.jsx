import styled from "styled-components";

const StatusContainer = styled.div`
  position: absolute;
  width: 50%;
  height: 33%;
  top: 0px;
  right: 0px;

  overflow: hidden;

  border-bottom: solid 1px red;
  border-left: solid 1px red;
`;

const StatusText = styled.div`
  position: absolute;
  bottom: 0px;

  color: white;
  font-family: IBM3270;
  font-size: 1.1vw;

  margin-bottom: 1vh;
  margin-left: 1vw;
`;

export { StatusContainer, StatusText };
