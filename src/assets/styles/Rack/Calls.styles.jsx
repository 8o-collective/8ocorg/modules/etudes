import styled from "styled-components";

const CallsContainer = styled.div`
  position: absolute;
  width: 50%;
  height: 33%;
  top: 0px;
  left: 0px;

  overflow: hidden;

  border-bottom: solid 1px red;
`;

const CallsText = styled.div`
  position: absolute;
  bottom: 0px;

  color: red;
  font-family: IBM3270;
  font-size: 1.1vw;

  margin-bottom: 1vh;
  margin-left: 1vw;
`;

export { CallsContainer, CallsText };
