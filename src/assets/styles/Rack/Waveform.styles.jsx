import React from "react";
import styled from "styled-components";

const WaveformContainer = styled.div`
  position: absolute;
  width: 100%;
  height: 66%;
  bottom: 0px;

  //   border: solid blue 1px;
`;

const WaveformCanvas = styled.canvas`
  position: absolute;
  width: 100%;
  height: 100%;
`;

const WaveformAudio = styled.audio`
  display: none;
`;

const WaveformInformationContainer = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: 1;

  color: red;
  font-family: IBM3270;
  font-size: 4vw;
`;

const WaveformBPM = styled.div`
  position: absolute;
  right: 0px;
  top: 0px;

  margin: 0.3vw 1vw;
`;

const WaveformSampleRate = styled.div`
  position: absolute;
  left: 0px;
  top: 0px;

  margin: 0.3vw 1vw;
`;

const WaveformAmplitude = styled.div`
  position: absolute;
  right: 0px;
  bottom: 0px;

  margin: 0.3vw 1vw;
`;

const WaveformInformation = ({ bpm, samplerate, amplitude }) => (
  <WaveformInformationContainer>
    <WaveformBPM>{bpm}</WaveformBPM>
    <WaveformSampleRate>{samplerate}</WaveformSampleRate>
    <WaveformAmplitude>{amplitude}</WaveformAmplitude>
  </WaveformInformationContainer>
);

export {
  WaveformContainer,
  WaveformCanvas,
  WaveformAudio,
  WaveformInformation,
};
