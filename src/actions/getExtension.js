import constants from "assets/constants";

// let cookies = Object.fromEntries(
//   document.cookie.split(/; */).map((cookie) => cookie.split("=", 2))
// );

// const getExtension = () =>
//   cookies.id
//     .slice(-4)
//     .split("")
//     .map((letter) =>
//       isNaN(letter) ? letter : String.fromCharCode(96 + parseInt(letter))
//     )
//     .join("");

const getExtension = () => constants.globalExt;

const extension = getExtension();

export { extension, getExtension };
