import MidiPlayer from "midi-player-js";

import constants from "assets/constants";

ArrayBuffer.prototype.read = function (byteAmount) {
  if (this.readHead === undefined) this.readHead = 0;

  const bytes = this.slice(this.readHead, this.readHead + byteAmount);

  this.readHead += byteAmount;

  return bytes;
};

const equal = (first, second) =>
  first.length === second.length &&
  first.every((value, index) => value === second[index]);

const checkFile = (data) => {
  // file header spec
  // we start with the 8-byte fileHeader, to verify that the next bytes really are valid data
  // the following is in the format [description](amount of bytes)
  // [file header](8) [audio size](4) [midi size](4) [comment csv size](4)
  // 20 bytes or 160 bits in total

  // console.log(data)

  let header = new Uint8Array(data.read(constants.fileHeader.byteLength));

  if (!equal(header, constants.fileHeader))
    throw "CANNOT LOAD FILE DUE TO HEADER MISMATCH";

  const sizes = constants.order.reduce(
    (o, file) => ({ ...o, [file]: new DataView(data.read(4)).getUint32() }),
    {}
  );

  let totalSize = constants.headerSize;

  for (const [key, value] of Object.entries(sizes)) {
    if (value === 0) throw `${key.toUpperCase()} CANNOT BE SIZE 0`;

    totalSize += value;
  }

  if (data.byteLength != totalSize)
    throw "FILE SIZE DOES NOT MATCH SIZES LISTED IN HEADER";

  return Object.entries(sizes).reduce((individualData, pair) => {
    const [key, value] = pair;
    individualData[key] = data.read(value);

    return individualData;
  }, {});
};

const loadCSV = (data) => {
  const [headerLine, ...lines] = data.split("\n");

  const parseLine = (line) => JSON.parse(`[${line}]`);

  const headers = parseLine(headerLine);

  const objects = lines.map((line) =>
    parseLine(line).reduce(
      (object, value, index) => ({
        ...object,
        [headers[index]]: value,
      }),
      {}
    )
  );

  return objects;
};

const loadFile = (data, pushStatus, pushCall, setData, setInformation) => {
  const containsEventData = (e) =>
    e.time !== undefined && e.message !== undefined;

  const comments = loadCSV(
    new TextDecoder("utf-8").decode(data.comments)
  ).filter(containsEventData);

  console.log(data.midi);

  let state = {};

  const Player = new MidiPlayer.Player((e) => {
    if (e.name === "Set Tempo") {
      pushCall(`setTempo(${e.data}, &StateProvider)`);
      pushCall(`calculateAudioMatrix(memset(StateProvider, NULL))`);
      setInformation((prev) => ({ ...prev, bpm: e.data }));
    }

    if (e.name === "Note on") {
      if (state[e.noteNumber] === undefined)
        pushCall(`calculatePitch(${e.noteNumber}, &Track[${e.track}])`);
      state[e.noteNumber] = parseInt(Math.random() * 1000000);
      pushCall(
        `createAudio(${e.noteNumber}, *AudioContext[${state[e.noteNumber]}])`
      );
    }

    if (e.name === "Note off") {
      if (Math.random() < 0.1) {
        pushCall(
          `swapController(&StateProvider->controller, *AudioContext, ${e.track})`
        );
      }

      pushCall(`killAudio(*AudioContext[${state[e.noteNumber]}])`);
    }

    // console.log(e)
  });

  Player.on("fileLoaded", () => {
    const loadTimeouts = (events) =>
      events.map((e) => setTimeout(() => pushStatus(e.message), e.time));

    const timeouts = loadTimeouts(comments);
    console.log(timeouts);

    const audioBlob = new Blob([new Uint8Array(data.audio)], {
      type: "audio/ogg",
    });

    setData((prevData) => ({
      ...prevData,
      raw: URL.createObjectURL(audioBlob),
      context: new window.AudioContext(),
    }));

    Player.play();
  });

  Player.on("endOfFile", () => console.log("end"));

  Player.loadArrayBuffer(data.midi);
};

export { checkFile, loadFile };
