import constants from "assets/constants.js";

const generateFile = async (files) => {
  // audio, midi, comments
  const readFileAsync = (file) =>
    new Promise((resolve) => {
      const reader = new FileReader();
      reader.onload = (e) => resolve(e.target.result);
      reader.readAsArrayBuffer(file);
    });

  return Promise.all(
    Object.values(files).map((file) => readFileAsync(file))
  ).then(
    (data) =>
      new Blob([
        constants.fileHeader,
        ...data.map((buffer) =>
          new Uint8Array(new Uint32Array([buffer.byteLength]).buffer).reverse()
        ),
        ...data,
      ])
  );
};

export { generateFile };
